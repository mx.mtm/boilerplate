var DEBUG = window.location.href.indexOf('__debug') > 0,
    _l = function (o) {
        if (DEBUG) console.log(o)
    },

    mainApp = angular.module('mainApp', ['ngResource'])

        .controller(
        'MainController',
        ['ApiService', MainController]
    )

        .factory(
        'ApiService',
        [
            '$resource',
            function ($resource) {
                return $resource('/api');
            }
        ])

    ;


function MainController(ApiService) {
    var that = this;

    this.dataReady = false;
    this.user = {
        name: 'John S.'
    };


    ApiService.get(function (data) {
        if (data.status) {
            that.dataReady = true;
        }
    });
}


