package mx.mtm.boilerplate.controllers;

import mx.mtm.common.BasicResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author qtoosha
 * @since 2015.11.08
 */
@RestController
@RequestMapping("/api")
public class ApiController {

    @RequestMapping
    public HttpEntity<BasicResponse> index() {
        return new ResponseEntity<BasicResponse>(new BasicResponse(true, "Hello, world"), HttpStatus.OK);
    }
}
