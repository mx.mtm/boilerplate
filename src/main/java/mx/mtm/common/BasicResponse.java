package mx.mtm.common;

import lombok.Getter;
import lombok.Setter;

/**
 * @author qtoosha
 * @since 2015.10.18
 */
@Getter
@Setter
public class BasicResponse {

    /**
     * Status
     */
    public boolean status;

    /**
     * Description
     */
    public String description;

    /**
     * Basic controller
     *
     * @param status      Status
     * @param description Description
     */
    public BasicResponse(boolean status, String description) {
        this.status = status;
        this.description = description;
    }
}
